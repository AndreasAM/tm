#![macro_escape]

use std::collections::HashMap;
use std::hash::Hash;
use tape;
use Direction;


pub struct Tm<A, Q> {
    start_state: Q,
    final_state: Q,
    transitions: HashMap<(Q, Option<A>), (Q, Option<A>, Direction)>,
    state: Q,
    tape: tape::Tape<A>
}

impl<A, Q> Tm<A, Q>
where A: Clone + Eq + Hash, Q: Clone + Eq + Hash {
    pub fn new(start_state: Q, final_state: Q) -> Tm<A, Q> {
        Tm {
            start_state: start_state.clone(),
            final_state: final_state,
            transitions: HashMap::new(),
            state: start_state,
            tape: tape::Tape::new()
        }
    }

    pub fn from_transitions(start_state: Q, final_state: Q, transitions: HashMap<(Q, Option<A>), (Q, Option<A>, Direction)>) -> Tm<A, Q> {
        Tm {
            start_state: start_state.clone(),
            final_state: final_state,
            transitions: transitions,
            state: start_state,
            tape: tape::Tape::new()
        }
    }

    pub fn add_transition(&mut self, q0: Q, a0: Option<A>, q1: Q, a1: Option<A>, dir: Direction) {
        self.transitions.insert((q0, a0), (q1, a1, dir));
    }

    pub fn step(&mut self) {
        let d = {
            let &(ref q, ref a, d) = {
                let q = &self.state;
                let a = self.tape.read();
                self.transitions.find_equiv(&StateEquiv(q, a)).unwrap()
            };

            self.state = q.clone();
            self.tape.write(a.clone());
            d
        };
        self.tape.move_head(d);
    }

    pub fn execute(&mut self, input: &[A]) -> Vec<A> {
        self.state = self.start_state.clone();
        self.tape = tape::Tape::from_input(input);

        while self.state != self.final_state {
            self.step();
        }

        self.tape.output()
    }
}

#[deriving(Hash)]
struct StateEquiv<'a, Q: 'a, A: 'a>(&'a Q, &'a A);

impl<'a, Q, A> Equiv<(Q, A)> for StateEquiv<'a, Q, A>
where Q: Eq, A: Eq {
    fn equiv(&self, &(ref q2, ref a2): &(Q, A)) -> bool {
        let &StateEquiv(q1, a1) = self;
        (q1 == q2) && (a1 == a2)
    }
}


macro_rules! transition {
    ($tm:expr, $q0:expr, # -> $q1:expr, #, $dir:expr) => {
        $tm.add_transition($q0, None, $q1, None, $dir);
    };
    ($tm:expr, $q0:expr, # -> $q1:expr, $a1:expr, $dir:expr) => {
        $tm.add_transition($q0, None, $q1, Some($a1), $dir);
    };
    ($tm:expr, $q0:expr, $a0:expr -> $q1:expr, #, $dir:expr) => {
        $tm.add_transition($q0, Some($a0), $q1, None, $dir);
    };
    ($tm:expr, $q0:expr, $a0:expr -> $q1:expr, $a1:expr, $dir:expr) => {
        $tm.add_transition($q0, Some($a0), $q1, Some($a1), $dir);
    };
}
