use std::num::Bounded;
use std::fmt;
use Direction;
use NoDirection;
use Left;
use Right;


pub struct Tape<A> {
    right_tape: Vec<Option<A>>,
    left_tape: Vec<Option<A>>,
    position: uint
}

impl<A> Tape<A>
where A: Clone {
    pub fn new() -> Tape<A> {
        Tape {
            right_tape: vec![],
            left_tape: vec![],
            position: 0
        }
    }

    pub fn from_input(input: &[A]) -> Tape<A> {
        Tape {
            right_tape: input.iter().map(|a| Some(a.clone())).collect(),
            left_tape: vec![],
            position: 0
        }
    }

    pub fn move_head(&mut self, dir: Direction) {
        match dir {
            NoDirection => (),
            Left => {
                self.position -= 1;
                if !self.on_right_tape() {
                    let i = !self.position;
                    if i >= self.left_tape.len() {
                        let n = i - self.left_tape.len() + 1;
                        self.left_tape.grow_fn(n, |_| None);
                    }
                }
            }
            Right => {
                self.position += 1;
                if self.on_right_tape() {
                    if self.position >= self.right_tape.len() {
                        let n = self.position - self.right_tape.len() + 1;
                        self.right_tape.grow_fn(n, |_| None);
                    }
                }
            }
        }
    }

    pub fn read(&self) -> &Option<A> {
        if self.on_right_tape() {
            &self.right_tape[self.position]
        } else {
            &self.left_tape[!self.position]
        }
    }

    pub fn write(&mut self, a: Option<A>) {
        if self.on_right_tape() {
            self.right_tape[self.position] = a;
        } else {
            self.left_tape[!self.position] = a;
        }
    }

    pub fn output(&self) -> Vec<A> {
        if self.on_right_tape() {
            self.right_tape.slice_from(self.position).iter()
                .take_while(|a| a.is_some())
                .flat_map(|a| a.clone().into_iter())
                .collect()
        } else {
            let left_iter = self.left_tape.slice_to(!self.position + 1).iter().rev();
            let right_iter = self.right_tape.iter();
            left_iter.chain(right_iter)
                .take_while(|a| a.is_some())
                .flat_map(|a| a.clone().into_iter())
                .collect()
        }
    }

    fn on_right_tape(&self) -> bool {
        let max: int = Bounded::max_value();
        self.position <= max as uint
    }
}

impl<A> fmt::Show for Tape<A>
where A: fmt::Show {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut first = true;
        for a in self.left_tape.iter().rev() {
            if !first {
                write!(f, " ");
            } else {
                first = false;
            }
            match a {
                &Some(ref a) => write!(f, "{}", a),
                &None => write!(f, "B")
            };
        }

        write!(f, " | ");

        let mut first = true;
        for a in self.right_tape.iter() {
            if !first {
                write!(f, " ");
            } else {
                first = false;
            }
            match a {
                &Some(ref a) => write!(f, "{}", a),
                &None => write!(f, "B")
            };
        }
        Ok(())
    }
}
