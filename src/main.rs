#![feature(macro_rules, tuple_indexing)]

use tm::Tm;

mod tape;
mod tm;


enum Direction {
    NoDirection,
    Left,
    Right
}

#[deriving(Clone, PartialEq, Eq, Hash)]
enum AddingStates {
    GoRight,
    NoCarry,
    Carry,
    End
}


fn main() {
    let mut tm = Tm::new(GoRight, End);

    transition!(tm, GoRight, (0, 0, 0) -> GoRight, (0, 0, 0), Right);
    transition!(tm, GoRight, (0, 1, 0) -> GoRight, (0, 1, 0), Right);
    transition!(tm, GoRight, (1, 0, 0) -> GoRight, (1, 0, 0), Right);
    transition!(tm, GoRight, (1, 1, 0) -> GoRight, (1, 1, 0), Right);
    transition!(tm, GoRight, #         -> NoCarry, #,         Left);

    transition!(tm, NoCarry, (0, 0, 0) -> NoCarry, (0, 0, 0), Left);
    transition!(tm, NoCarry, (0, 1, 0) -> NoCarry, (0, 1, 1), Left);
    transition!(tm, NoCarry, (1, 0, 0) -> NoCarry, (1, 0, 1), Left);
    transition!(tm, NoCarry, (1, 1, 0) -> Carry,   (1, 1, 0), Left);
    transition!(tm, NoCarry, #         -> End,     #,         Right);

    transition!(tm, Carry, (0, 0, 0) -> NoCarry, (0, 0, 1), Left);
    transition!(tm, Carry, (0, 1, 0) -> Carry,   (0, 1, 0), Left);
    transition!(tm, Carry, (1, 0, 0) -> Carry,   (1, 0, 0), Left);
    transition!(tm, Carry, (1, 1, 0) -> Carry,   (1, 1, 1), Left);
    transition!(tm, Carry, #         -> End,     (0, 0, 1), NoDirection);

    let input1 = vec![1u8, 0, 0, 1];
    let input2 = vec![1u8, 1, 1, 0];
    let input: Vec<(u8, u8, u8)> = input1.iter().zip(input2.iter())
        .map(|(x, y)| (*x, *y, 0)).collect();
    let output = tm.execute(input.as_slice());

    let output: Vec<_> = output.into_iter().map(|x| x.2).collect();
    println!("{} + {} = {}", input1, input2, output);
}
